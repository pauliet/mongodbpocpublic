package com.pauliet.mongodbpoc.entity

import groovy.transform.ToString
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@ToString
@Document(collection = 'Users')
class UserMongoDbDocument {

    @Id
    private String id
    private String name
    private String surname

    @Override
    String toString() {
        "$surname $name"
    }
}
