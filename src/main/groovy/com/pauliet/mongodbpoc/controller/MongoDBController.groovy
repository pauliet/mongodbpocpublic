package com.pauliet.mongodbpoc.controller

import com.pauliet.mongodbpoc.model.User
import com.pauliet.mongodbpoc.service.MongoDBService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController

@RestController
class MongoDBController {

    @Autowired
    MongoDBService mongoDBService

    @RequestMapping(value = '/allusers',
                    method = RequestMethod.GET,
                    produces = 'application/json')
    @ResponseBody
    List<User> getAllUsers() {
        mongoDBService.getAllUses()
    }
}
