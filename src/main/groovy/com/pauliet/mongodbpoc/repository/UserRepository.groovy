package com.pauliet.mongodbpoc.repository

import com.pauliet.mongodbpoc.entity.UserMongoDbDocument
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.mongodb.repository.Query
import org.springframework.stereotype.Repository

@Repository
interface UserRepository extends MongoRepository<UserMongoDbDocument, String> {
    UserMongoDbDocument findFirstByName(String name)

    @Query("{surname:'?0'}")
    List<UserMongoDbDocument> findUsersBySurname(String surname)
}