package com.pauliet.mongodbpoc.model

import groovy.transform.CompileStatic

@CompileStatic
class User {
    String id, name, surname
}
