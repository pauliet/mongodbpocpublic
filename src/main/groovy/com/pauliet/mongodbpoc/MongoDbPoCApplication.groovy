package com.pauliet.mongodbpoc

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.core.env.Environment

@SpringBootApplication
//@EnableAutoConfiguration(exclude=[MongoAutoConfiguration])
class MongoDbPoCApplication {

	@Autowired
	static Environment environment

	static void main(String[] args) {

		println "HEEEEE ${environment.getProperties()}"
		SpringApplication.run(MongoDbPoCApplication, args)
	}

}
