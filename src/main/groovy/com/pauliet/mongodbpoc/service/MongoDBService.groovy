package com.pauliet.mongodbpoc.service

import com.pauliet.mongodbpoc.entity.UserMongoDbDocument
import com.pauliet.mongodbpoc.model.User
import com.pauliet.mongodbpoc.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MongoDBService {

    @Autowired
    private UserRepository userRepository

    void addSampleData() {
        userRepository.save(new UserMongoDbDocument(name: 'Tiny', surname: 'Thomas'))
    }

    List<User> getAllUses() {
        List<User> users = []
        List<UserMongoDbDocument> mongoDbDocumentList = userRepository.findAll()
        mongoDbDocumentList.each {
            users << new User(name: it.name, surname: it.surname, id: it.id)
        }
        users
    }
}
