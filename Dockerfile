FROM alpine:latest
MAINTAINER paulie.thomas@icloud.com
RUN apk add --no-cache openjdk11
COPY build/libs/MongoDBPoC-0.0.1-SNAPSHOT.jar /opt/spring-cloud/lib/
ENTRYPOINT ["/usr/bin/java"]
CMD ["-jar", "/opt/spring-cloud/lib/MongoDBPoC-0.0.1-SNAPSHOT.jar"]
VOLUME /var/lib/spring-cloud
EXPOSE 20005

# To build the image:-> docker build -t mongo_db_poc .
# To run the container injecting a spring profile:-> docker run -p 20005:8080 mongo_db_poc